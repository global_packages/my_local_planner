/*********************************************************************
 *
 * Software License Agreement (BSD License)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 * Author: Puneet Tiwari
 *********************************************************************/
#ifndef MY_LOCAL_PLANNER_ROS_H_
#define MY_LOCAL_PLANNER_ROS_H_

#include <Eigen/Core>
#include <cmath>
#include <angles/angles.h>

#include <tf2/utils.h>
#include <tf2_ros/buffer.h>
#include <tf/transform_listener.h>

#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <nav_core/base_local_planner.h>
#include <nav_core/parameter_magic.h>

#include <costmap_2d/costmap_2d_ros.h>

#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Point.h>

#include <base_local_planner/latched_stop_rotate_controller.h>
#include <base_local_planner/trajectory.h>
#include <base_local_planner/local_planner_limits.h>
#include <base_local_planner/local_planner_util.h>
#include <base_local_planner/simple_trajectory_generator.h>
#include <base_local_planner/goal_functions.h>
#include <base_local_planner/odometry_helper_ros.h>

namespace my_local_planner
{
  /**
   * @class MYPlannerROS
   * @brief ROS Wrapper that adheres to the
   * BaseLocalPlanner interface and can be used as a plugin for move_base.
   */
  class MYPlannerROS : public nav_core::BaseLocalPlanner
  {
  public:
    /**
     * @brief  Constructor for MYPlannerROS wrapperotherwise
     */
    MYPlannerROS();

    /**
     * @brief  Constructs the ros wrapper
     * @param name The name to give this instance of the trajectory planner
     * @param tf A pointer to a transform listener
     * @param costmap The cost map to use for assigning costs to trajectories
     */
    void initialize(std::string name, tf2_ros::Buffer *tf,
                    costmap_2d::Costmap2DROS *costmap_ros);

    /**
     * @brief  Destructor for the wrapper
     */
    ~MYPlannerROS();

    /**
     * @brief  Given the current position, orientation, and velocity of the robot,
     * compute velocity commands to send to the base
     * @param cmd_vel Will be filled with the velocity command to be passed to the robot base
     * @return True if a valid trajectory was found, false otherwise
     */
    bool computeVelocityCommands(geometry_msgs::Twist &cmd_vel);

    /**
     * @brief  Set the plan that the controller is following
     * @param orig_global_plan The plan to pass to the controller
     * @return True if the plan was updated successfully, false otherwise
     */
    bool setPlan(const std::vector<geometry_msgs::PoseStamped> &orig_global_plan);

    /**
     * @brief  Check if the goal pose has been achieved
     * @return True if achieved, false otherwise
     */
    bool isGoalReached();

    bool isInitialized()
    {
      return initialized_;
    }

  private:

    tf2_ros::Buffer *tf_; ///< @brief Used for transforming point clouds

    base_local_planner::LocalPlannerUtil planner_util_;

    costmap_2d::Costmap2DROS *costmap_ros_;

    geometry_msgs::PoseStamped current_pose_;

    base_local_planner::LatchedStopRotateController latchedStopRotateController_;

    bool setup_;

    // true if the planner has been initialized successfully
    bool initialized_;

    // true if the goal point is reache and orientation of goal is reached
    bool goal_reached_;

    // x velocity of the previous round
    double cmd_vel_linear_x_;
    // rotation velocity of previous round for the dirveToward methode
    double cmd_vel_angular_z_;

    // all the configuration parameters
    double max_forward_vel_;
    double forward_point_distance_;
    double max_rotation_vel_;
    double max_acc_limit_trans_;
    double max_acc_limit_rot_;
    double transitional_tol_;
    double rotational_tol_;
    double sim_time_;
    double sim_granularity_;
    double look_ahead_;

    // global plan which we run along
    std::vector<geometry_msgs::PoseStamped> global_plan_;

    // last point of the global plan in global frame
    tf::Stamped<tf::Pose> goal_pose_;

    std::string odom_topic_;

    base_local_planner::OdometryHelperRos odom_helper_;

    base_local_planner::LocalPlannerLimits limits;

    base_local_planner::SimpleTrajectoryGenerator generator_;
  };
};
#endif
