my_local_planner ROS Package
=============================

This package provides an implementation of the simple geometry planner to local robot navigation on a 2D navigation plane. Given a global plan to follow and a costmap, the local planner produces velocity commands to send to a mobile base.

## License

The *my_local_planner* package is licensed under the BSD license.
It depends on other ROS packages, which are listed in the package.xml. They are also BSD licensed.

Some third-party dependencies are included that are licensed under different terms:
 - *Eigen*, MPL2 license, http://eigen.tuxfamily.org

All packages included are distributed in the hope that they will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the licenses for more details.

## Requirements

> Install `ROS Noetic` full desktop version or `sudo apt-get install ros-noetic-navigation ros-noetic-turtlebot3` 

> Install dependencies (listed in the *package.xml* and *CMakeLists.txt* file) using *rosdep*: `rosdep install my_local_planner`


## Process e-Manual

Run the following commands on separate terminals for smooth execution
1. Create a catkin workspace and within its `src` folder copy & paste `my_local_planner` package.
2. After complete installation run `catkin build` and source it.
3. After Successful build, run following commands on separate terminals:
    3.1 `roscore`
    3.2 `export TURTLEBOT3_MODEL=burger`
    3.3 `roslaunch my_local_planner turtlebot3_gazebo.launch`
    3.4 `roslaunch my_local_planner turtlebot3_navigation.launch`
