/*********************************************************************
 *
 * Software License Agreement (BSD License)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 * Author: Puneet Tiwari
 *********************************************************************/

#include "my_local_planner/my_planner_ros.h"

#include <pluginlib/class_list_macros.h>

// Register the planner as a BaseLocalPlanner plugin so as to use it with move base
PLUGINLIB_EXPORT_CLASS(my_local_planner::MYPlannerROS, nav_core::BaseLocalPlanner)

namespace my_local_planner
{

  MYPlannerROS::MYPlannerROS() : initialized_(false),
                                 odom_helper_("odom"), setup_(false)
  {
  } // End of Constructor

  void MYPlannerROS::initialize(
      std::string name,
      tf2_ros::Buffer *tf,
      costmap_2d::Costmap2DROS *costmap_ros)
  {
    if (!isInitialized())
    {
      // Private NodeHandle initiation for multiple node instance handling
      ros::NodeHandle nh_p_("~/" + name);

      if (nh_p_.getParam("odom_topic", odom_topic_))
      {
        odom_helper_.setOdomTopic(odom_topic_);
      }

      // Initialize the required Parameters
      nh_p_.param<double>("max_forward_vel", max_forward_vel_, 0.2);
      ROS_WARN("Setting Max Forward Velocity to : %lf", max_forward_vel_);

      limits.max_vel_x = max_forward_vel_;
      limits.min_vel_x = -max_forward_vel_;
      limits.max_vel_y = 0.0;
      limits.min_vel_y = 0.0;
      limits.max_vel_trans = max_forward_vel_;     // The velocity when robot is moving forward in a straight line
      limits.min_vel_trans = max_forward_vel_ / 2; // The velocity when robot is moving reverse in a straight line

      nh_p_.param<double>("max_rotation_vel", max_rotation_vel_, 0.1);
      limits.max_vel_theta = max_rotation_vel_;
      limits.min_vel_theta = -max_rotation_vel_;
      ROS_WARN("Setting Max Rotation Velocity to : %lf", max_rotation_vel_);

      nh_p_.param<double>("max_acc_limit_trans", max_acc_limit_trans_, 1.5);
      limits.acc_lim_x = max_acc_limit_trans_;
      limits.acc_lim_trans = max_acc_limit_trans_;
      limits.acc_lim_y = 0.0;
      ROS_WARN("Setting Max Acceleration Limit X to : %lf", max_acc_limit_trans_);

      nh_p_.param<double>("max_acc_limit_rot", max_acc_limit_rot_, 1.2);
      limits.acc_lim_theta = max_acc_limit_rot_;
      ROS_WARN("Setting Max Rotation Acceleration to : %lf", max_acc_limit_rot_);

      nh_p_.param<double>("transitional_tol", transitional_tol_, 0.0005);
      nh_p_.param<double>("rotational_tol", rotational_tol_, 0.05);
      limits.xy_goal_tolerance = transitional_tol_;
      limits.yaw_goal_tolerance = rotational_tol_;
      ROS_WARN("Setting Goal Tolerances Transitional to : %lf and Rotational to : %lf", transitional_tol_, rotational_tol_);

      nh_p_.param<double>("sim_time", sim_time_, 0.8);
      ROS_WARN("Setting Simulation Time to : %lf", sim_time_);
      
      nh_p_.param<double>("sim_granularity", sim_granularity_, 0.8);
      ROS_WARN("Setting Simulation Granularity to : %lf", sim_granularity_);

      // Lower the value, higher the accuracy, however it might result unstable robot behaviour
      nh_p_.param<double>("look_ahead", look_ahead_, 10);
      ROS_WARN("Setting Look Ahead distance to : %lf", look_ahead_);

      goal_reached_ = false;
      cmd_vel_linear_x_ = 0;
      cmd_vel_angular_z_ = 0;

      // Get the global TF tree
      tf_ = tf;
      // Get CostMap
      costmap_ros_ = costmap_ros;
      // Get the Robot Pose in Global Frame
      costmap_ros_->getRobotPose(current_pose_);
      // make sure to update the costmap we'll use for this cycle
      costmap_2d::Costmap2D *costmap = costmap_ros_->getCostmap();
      // Initialization of the planner Utilies with TF, costmap and GlobalFrame
      planner_util_.initialize(tf, costmap, costmap_ros_->getGlobalFrameID());

      // angular_sim_granularity = 1 and sim_period = 0.1 (Need to run at 10Hz)
      generator_.setParameters(
          sim_time_,
          sim_granularity_,
          1,
          false,
          0.1);

      initialized_ = true;
      ROS_INFO("Initializing the MyLocalPlanner");
    }
    else
    {
      ROS_WARN("This planner has already been initialized, doing nothing.");
    }
  } // End Of function initialize

  bool MYPlannerROS::setPlan(const std::vector<geometry_msgs::PoseStamped> &orig_global_plan)
  {
    if (!isInitialized())
    {
      ROS_ERROR("MyLocalPlanner has not been initialized, please call initialize() before using this planner");
      return false;
    }
    // On new Path Receival, clear any latch on goal tolerances
    latchedStopRotateController_.resetLatching();

    // store global plan
    global_plan_.clear();
    global_plan_ = orig_global_plan;

    // we do not clear the local planner here, since setPlan is called frequently whenever the global planner updates the plan.
    // the local planner checks whether it is required to reinitialize the trajectory or not within each velocity computation step.
    ROS_INFO("Got new plan");

    // reset goal_reached_ flag
    goal_reached_ = false;

    return planner_util_.setPlan(orig_global_plan);
  } // End Of function set Plan

  bool MYPlannerROS::isGoalReached()
  {
    if (!isInitialized())
    {
      ROS_ERROR("This planner has not been initialized, please call initialize() before using this planner");
      return false;
    }
    if (!costmap_ros_->getRobotPose(current_pose_))
    {
      ROS_ERROR("Could not get robot pose");
      return false;
    }

    if (goal_reached_)
    {
      ROS_INFO("MyPlannerPlanner: Goal reached.");
    }
    return goal_reached_;
  } // End Of function Goal Reached

  MYPlannerROS::~MYPlannerROS()
  {
    // make sure to clean things up here
  }

  bool MYPlannerROS::computeVelocityCommands(geometry_msgs::Twist &cmd_vel)
  {
    // dispatches to either sampling control or stop and rotate control, depending on whether we have been close enough to goal
    if (!costmap_ros_->getRobotPose(current_pose_))
    {
      ROS_ERROR("Could not get robot pose");
      return false;
    }
    std::vector<geometry_msgs::PoseStamped> transformed_plan;
    if (!planner_util_.getLocalPlan(current_pose_, transformed_plan))
    {
      ROS_ERROR("Could not get local plan");
      return false;
    }

    // if the global plan passed in is empty... we won't do anything
    if (transformed_plan.empty())
    {
      ROS_WARN_NAMED("my_local_planner", "Received an empty transformed plan.");
      return false;
    }

    std::vector<double> localCoordiantes;

    // getLocalCoordinates
    double x_global = current_pose_.pose.position.x;
    double y_global = current_pose_.pose.position.y;
    double x_goal_global;
    double y_goal_global;
    double theta_global;
    if (look_ahead_ < global_plan_.size() - 1)
    {
      x_goal_global = global_plan_[look_ahead_].pose.position.x;
      y_goal_global = global_plan_[look_ahead_].pose.position.y;
      theta_global = tf2::getYaw(global_plan_[look_ahead_].pose.orientation);
    }
    else
    {
      x_goal_global = global_plan_[global_plan_.size() - 1].pose.position.x;
      y_goal_global = global_plan_[global_plan_.size() - 1].pose.position.y;
      theta_global = tf2::getYaw(global_plan_[global_plan_.size() - 1].pose.orientation);
    }

    double yaw = tf::getYaw(current_pose_.pose.orientation);

    localCoordiantes.push_back((x_goal_global - x_global) * cos(-yaw) - (y_goal_global - y_global) * sin(-yaw));
    localCoordiantes.push_back((x_goal_global - x_global) * sin(-yaw) + (y_goal_global - y_global) * cos(-yaw));

    double distance_square = localCoordiantes[0] * localCoordiantes[0] + localCoordiantes[1] * localCoordiantes[1];
    cmd_vel_linear_x_ = 0.5 * (sqrt(distance_square));
    cmd_vel_angular_z_ = 0.5 * ((2 * localCoordiantes[1] / (distance_square)));
    
    cmd_vel.angular.z = cmd_vel_angular_z_;
    cmd_vel.linear.x = cmd_vel_linear_x_;

    double distanceToGoal =
        (current_pose_.pose.position.x - global_plan_[global_plan_.size() - 1].pose.position.x) * (current_pose_.pose.position.x - global_plan_[global_plan_.size() - 1].pose.position.x) +
        (current_pose_.pose.position.y - global_plan_[global_plan_.size() - 1].pose.position.y) * (current_pose_.pose.position.y - global_plan_[global_plan_.size() - 1].pose.position.y);

    double delta_orient = theta_global - tf2::getYaw(current_pose_.pose.orientation);

    if (std::fabs(distanceToGoal < transitional_tol_))
    {
      cmd_vel.linear.x = 0;
      cmd_vel.linear.y = 0;

      // cmd angular remains the same
      std::cout << "Achieved GoalXY\n";

      if (std::fabs(delta_orient < rotational_tol_))
      {
        goal_reached_ = true;
        cmd_vel.angular.z = 0;
      }
    }
    return true;
  } // End of Compute Velocity Function
}; // End of NameSpace
